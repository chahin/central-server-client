/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

import React, { useState, useEffect } from "react";
import { Tab, Button, Nav, OverlayTrigger, Tooltip } from "react-bootstrap";
import { Link, useHistory, useLocation } from "react-router-dom";
import { Home, Map, Edit } from ".";
import { store } from "../service";
import SettingsRoundedIcon from "@material-ui/icons/SettingsRounded";
import LocationOnRoundedIcon from "@material-ui/icons/LocationOnRounded";
import HomeRoundedIcon from "@material-ui/icons/HomeRounded";
import ExitToAppRoundedIcon from "@material-ui/icons/ExitToAppRounded";

const Main = function () {
  const history = useHistory();
  const location = useLocation();
  const [tab, setTab] = useState(location.pathname);

  useEffect(() => {
    setTab(location.pathname);
  }, [location]);

  const buttonStyle = {
    width: "45px",
    height: "45px"
  };

  const signOut = function (e) {
    e.preventDefault();
    store.clear();
    history.push("/");
  };

  const renderTooltip = function (props, text) {
    return (
      <Tooltip id="button-tooltip" {...props}>
        {text}
      </Tooltip>
    );
  }

  return <Tab.Container id="left-tabs-example" activeKey={tab} transition={false}>
    <div className="vh-100 bg-danger d-flex justify-content-between">
      <div className="h-100 bg-white" style={{ width: "75px" }}>
        <div className="d-flex flex-column align-items-center pt-3">
          <Nav variant="pills" className="flex-column">
            <Nav.Item className="mb-3" style={buttonStyle}>
              <Link className="text-decoration-none" to="/home">
                <OverlayTrigger
                  placement="right"
                  delay={{ show: 500, hide: 0 }}
                  overlay={(props) => renderTooltip(props, "Home")}
                >
                  <Nav.Link eventKey="/home" as={Button} variant="light" className="rounded-extra p-0" style={buttonStyle}>
                    <HomeRoundedIcon />
                  </Nav.Link>
                </OverlayTrigger>
              </Link>
            </Nav.Item>
            <hr className="m-0 mb-3 border-lighter" style={{ width: "45px" }} />
            <Nav.Item className="mb-3">
              <Link className="text-decoration-none" to="/studio">
                <OverlayTrigger
                  placement="right"
                  delay={{ show: 500, hide: 0 }}
                  overlay={(props) => renderTooltip(props, "Studio")}
                >
                  <Nav.Link eventKey="/studio" as={Button} variant="light" className="rounded-extra p-0" style={buttonStyle}>
                    <LocationOnRoundedIcon />
                  </Nav.Link>
                </OverlayTrigger>
              </Link>
            </Nav.Item>
            <Nav.Item className="mb-3">
              <Link className="text-decoration-none" to="/settings">
                <OverlayTrigger
                  placement="right"
                  delay={{ show: 500, hide: 0 }}
                  overlay={(props) => renderTooltip(props, "Settings")}
                >
                  <Nav.Link eventKey="/settings" as={Button} variant="light" className="rounded-extra p-0" style={buttonStyle}>
                    <SettingsRoundedIcon />
                  </Nav.Link>
                </OverlayTrigger>
              </Link>
            </Nav.Item>
            <Nav.Item className="mb-3">
              <a href="##" className="text-decoration-none" onClick={signOut}>
                <OverlayTrigger
                  placement="right"
                  delay={{ show: 500, hide: 0 }}
                  overlay={(props) => renderTooltip(props, "Sign out")}
                >
                  <Nav.Link as={Button} variant="light" className="rounded-extra p-0" style={buttonStyle}>
                    <ExitToAppRoundedIcon />
                  </Nav.Link>
                </OverlayTrigger>
              </a>
            </Nav.Item>
          </Nav>
        </div>
      </div>
      <div className="h-100 flex-grow-1 bg-lighter">
        <Tab.Content className="h-100">
          <Tab.Pane eventKey="/home" className="h-100">
            <Home />
          </Tab.Pane>
          <Tab.Pane eventKey="/studio" className="h-100">
            <Map />
          </Tab.Pane>
          <Tab.Pane eventKey="/settings" className="h-100">
            <Edit />
          </Tab.Pane>
        </Tab.Content>
      </div>
    </div>
  </Tab.Container>
};

export default Main;