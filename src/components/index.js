/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

export { default as Welcome } from "./Welcome";
export { default as SignIn } from "./SignIn";
export { default as SignUp } from "./SignUp";
export { default as Main } from "./Main";
export { default as Home } from "./Home";
export { default as Edit } from "./Edit";
export { Map } from "./studio";