/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

import React, { useState, useEffect } from "react";
import { Button, Col, Form, Alert } from "react-bootstrap";
import { useHistory } from "react-router-dom";

import { Formik } from "formik";
import * as yup from "yup";
import api from "../service/api";

const SignUp = function () {
  const [isError, setIsError] = useState(false);
  const [info, setInfo] = useState(null);
  const history = useHistory();

  const handleSubmit = function (values, { setSubmitting, resetForm }) {
    if (Object.values(values).find(e => e !== "")) {
      const { firstName, lastName, pseudo, emailId, password } = values;
      const data = {
        ...(firstName && { firstName }),
        ...(lastName && { lastName }),
        ...(pseudo && { pseudo }),
        ...(emailId && { emailId }),
        ...(password && { password }),
      }
      api.edit(data)
        .then(res => {
          setInfo({
            type: "success",
            message: "Account successfully updated"
          });
        }).catch(error => {
          setIsError("Something went wrong");
          // setSubmitting(false);
        });
    } else {
      setIsError("Please provide at least one field");
    }
    resetForm();
  };

  const showError = function () {
    return showAlert("Please provide at least one field", "danger", setIsError)
  };

  const showInfo = function () {
    return showAlert(info.message, info.type, setInfo);
  };

  const showAlert = function (message, variant, setValue) {
    setTimeout(() => {
      setValue(false);
    }, 5000);
    return <Alert className="m-0 ml-2 p-2" variant={variant}>
      {message}
    </Alert>
  };

  return <div className="h-100 overflow-auto p-3">
    <h4 className="font-weight-semi-bold m-0 py-2 mb-3">Edit</h4>
    <div className="p-3 bg-white rounded-extra w-50">
      <Formik
        validationSchema={yup.object({
          firstName: yup.string(),
          lastName: yup.string(),
          pseudo: yup.string(),
          emailId: yup.string(),
          password: yup.string().oneOf([yup.ref("confirmPassword"), null], "Passwords must match"),
          confirmPassword: yup.string().oneOf([yup.ref("password")], "Passwords must match").when('password', {
            is: (val) => val !== undefined,
            then: yup.string().required("Passwords must match"),
          })
        })}
        initialValues={{
          firstName: "",
          lastName: "",
          pseudo: "",
          emailId: "",
          password: "",
          confirmPassword: ""
        }}
        onSubmit={handleSubmit}
      >
        {({ handleSubmit, handleChange, handleBlur, values, touched, isValid, errors, }) => (
          <Form noValidate onSubmit={handleSubmit}>
            <Form.Row>
              <Form.Group as={Col}>
                <Form.Label>First name</Form.Label>
                <Form.Control
                  type="text"
                  name="firstName"
                  value={values.firstName}
                  onChange={handleChange}
                  isValid={touched.firstName && !errors.firstName && values.firstName}
                  isInvalid={touched.firstName && errors.firstName}
                />
              </Form.Group>
              <Form.Group as={Col}>
                <Form.Label>Last name</Form.Label>
                <Form.Control
                  type="text"
                  name="lastName"
                  value={values.lastName}
                  onChange={handleChange}
                  isValid={touched.lastName && !errors.lastName && values.lastName}
                  isInvalid={touched.lastName && errors.lastName}
                />
              </Form.Group>

            </Form.Row>
            <Form.Row>
              <Form.Group as={Col}>
                <Form.Label>Username</Form.Label>
                <Form.Control
                  type="text"
                  name="pseudo"
                  value={values.pseudo}
                  onChange={handleChange}
                  isValid={touched.pseudo && !errors.pseudo && values.pseudo}
                  isInvalid={touched.pseudo && errors.pseudo}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.pseudo}
                </Form.Control.Feedback>
              </Form.Group>

              <Form.Group as={Col}>
                <Form.Label>Email address</Form.Label>
                <Form.Control
                  type="email"
                  name="emailId"
                  value={values.emailId}
                  onChange={handleChange}
                  isValid={touched.emailId && !errors.emailId && values.emailId}
                  isInvalid={touched.emailId && errors.emailId}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.emailId}
                </Form.Control.Feedback>
              </Form.Group>
            </Form.Row>

            <Form.Row>
              <Form.Group as={Col}>
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  name="password"
                  value={values.password}
                  onChange={handleChange}
                  isValid={touched.password && !errors.password && values.password}
                  isInvalid={touched.password && errors.password}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.password}
                </Form.Control.Feedback>
              </Form.Group>

              <Form.Group as={Col}>
                <Form.Label>Confirm password</Form.Label>
                <Form.Control
                  type="password"
                  name="confirmPassword"
                  value={values.confirmPassword}
                  onChange={handleChange}
                  isValid={touched.confirmPassword && !errors.confirmPassword && values.confirmPassword}
                  isInvalid={touched.confirmPassword && errors.confirmPassword}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.confirmPassword}
                </Form.Control.Feedback>
              </Form.Group>
            </Form.Row>

            <div className="d-flex mb-2">
              <Button variant="warning" type="submit" className="p-2 font-weight-semi-bold">Submit</Button>
              {isError ? showError() : info && showInfo()}
            </div>
          </Form>
        )}
      </Formik>
    </div>
  </div>;
};

export default SignUp;