/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

import React, { useState, useEffect, useRef } from "react";
import { Button, Container, Row, Col, Form, Alert } from "react-bootstrap";
import { getImage } from "../assets/data";
import { Link, useHistory } from "react-router-dom";
import { Formik } from "formik";
import * as yup from "yup";
import { api, store } from "../service";

const SignUp = function () {
  const history = useHistory();
  const [info, setInfo] = useState(history.location.state && history.location.state.info);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    if (history.location.state && history.location.state.info) {
      const state = { ...history.location.state };
      delete state.info;
      history.replace({ ...history.location, state });
    }
  }, []);

  const handleSubmit = function (values, { setSubmitting, resetForm }) {
    api.signin(values)
      .then(res => {
        store.token = res.data;
        api.whoami().then(resUser => {
          const { data: user } = resUser;
          store.id = user.id;
          store.firstName = user.firstName;
          store.lastName = user.lastName;
          store.pseudo = user.pseudo;
          store.email = user.emailId;
          history.push("/home");
        }).catch(err => setIsError(true));
      }).catch(err => setIsError(true));
    resetForm();
  };

  const showError = function () {
    return showAlert("Wrong login or password", "danger", setIsError)
  };

  const showInfo = function () {
    return showAlert(info.message, info.type, setInfo);
  };

  const showAlert = function (message, variant, setValue) {
    setTimeout(() => {
      setValue(false);
    }, 5000);
    return <Alert className="m-0 ml-2 p-2" variant={variant}>
      {message}
    </Alert>
  };

  return <div className="vh-100 bg-black overflow-auto" style={{
    backgroundImage: `url(${getImage("noise.png")}), url(${getImage("pablo.png")})`,
    backgroundPosition: "top left, bottom right",
    backgroundRepeat: "repeat, no-repeat",
    backgroundSize: "cover, contain"
  }}>
    <div className="p-5 w-50">
      <div className="p-3 bg-white rounded-extra">
        <h3 className="mb-4 font-weight-semi-bold">Sign In</h3>
        <Formik
          onSubmit={handleSubmit}
          validationSchema={yup.object({
            pseudo: yup.string().required(true),
            password: yup.string().required(true)
          })}
          initialValues={{
            pseudo: "",
            password: "",
          }}
        >
          {({ handleSubmit, handleChange, handleBlur, values, touched, isValid, errors, }) => (
            <Form noValidate onSubmit={handleSubmit}>
              <Form.Group>
                <Form.Label>Username</Form.Label>
                <Form.Control
                  type="text"
                  name="pseudo"
                  value={values.pseudo}
                  onChange={handleChange}
                  isValid={touched.pseudo && !errors.pseudo}
                  isInvalid={touched.pseudo && errors.pseudo}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.pseudo}
                </Form.Control.Feedback>
              </Form.Group>

              <Form.Group>
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  name="password"
                  value={values.password}
                  onChange={handleChange}
                  isValid={touched.password && !errors.password}
                  isInvalid={touched.password && errors.password}
                />
                {/* <Form.Control.Feedback type="invalid">
                    {errors.password}
                  </Form.Control.Feedback> */}
              </Form.Group>

              <div className="d-flex mb-2">
                <Button variant="dark" type="submit" className="p-2">Submit</Button>
                {isError ? showError() : info && showInfo()}
              </div>
              <Link to="/signup" className="text-primary">
                Don't have an account? sign up
              </Link>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  </div>;
};

export default SignUp;