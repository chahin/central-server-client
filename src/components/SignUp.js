/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

import React, { useState, useEffect, useRef } from "react";
import { Button, Container, Row, Col, Form, Alert } from "react-bootstrap";
import { getImage } from "../assets/data";
import { Link, useHistory } from "react-router-dom";

import { Formik } from "formik";
import * as yup from "yup";
import api from "../service/api";

const SignUp = function () {
  const [isError, setIsError] = useState(false);
  const history = useHistory();


  const showAlert = function (message) {
    setTimeout(() => {
      setIsError(false);
    }, 5000);
    return <Alert className="m-0 ml-2 p-2" variant="danger">
      {message}
    </Alert>
  };

  return <div className="vh-100 bg-black overflow-auto" style={{
    backgroundImage: `url(${getImage("noise.png")}), url(${getImage("pablo.png")})`,
    backgroundPosition: "top left, bottom right",
    backgroundRepeat: "repeat, no-repeat",
    backgroundSize: "cover, contain"
  }}>
    <div className="p-5 w-50">
      <div className="p-3 bg-white rounded-extra">
        <h3 className="mb-4 font-weight-semi-bold">Sign Up</h3>
        <Formik
          validationSchema={yup.object({
            firstName: yup.string().required(true),
            lastName: yup.string().required(true),
            pseudo: yup.string().required(true),
            emailId: yup.string().required(true),
            password: yup.string().required(true),
            confirmPassword: yup.string().oneOf([yup.ref("password"), null], "Passwords must match").required(true)
          })}
          initialValues={{
            firstName: "",
            lastName: "",
            pseudo: "",
            emailId: "",
            password: "",
            confirmPassword: ""
          }}
          onSubmit={(values, { setSubmitting, resetForm }) => {
            const { firstName, lastName, pseudo, emailId, password } = values;
            api.signup({ firstName, lastName, pseudo, emailId, password })
              .then(res => {
                history.push("/signin", {
                  info: {
                    message: "Account successfully created",
                    type: "success"
                  }
                })
              }).catch(error => {
                setIsError(true);
                // setSubmitting(false);
              });
            resetForm();
          }}
        >
          {({ handleSubmit, handleChange, handleBlur, values, touched, isValid, errors, }) => (
            <Form noValidate onSubmit={handleSubmit}>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Label>First name</Form.Label>
                  <Form.Control
                    type="text"
                    name="firstName"
                    value={values.firstName}
                    onChange={handleChange}
                    isValid={touched.firstName && !errors.firstName}
                    isInvalid={touched.firstName && errors.firstName}
                  />
                  {/* <Form.Control.Feedback type="invalid">
                    {errors.firstName}
                  </Form.Control.Feedback> */}
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Label>Last name</Form.Label>
                  <Form.Control
                    type="text"
                    name="lastName"
                    value={values.lastName}
                    onChange={handleChange}
                    isValid={touched.lastName && !errors.lastName}
                    isInvalid={touched.lastName && errors.lastName}
                  />
                  {/* <Form.Control.Feedback type="invalid">
                    {errors.lastName}
                  </Form.Control.Feedback> */}
                </Form.Group>

              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Label>Username</Form.Label>
                  <Form.Control
                    type="text"
                    name="pseudo"
                    value={values.pseudo}
                    onChange={handleChange}
                    isValid={touched.pseudo && !errors.pseudo}
                    isInvalid={touched.pseudo && errors.pseudo}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.pseudo}
                  </Form.Control.Feedback>
                </Form.Group>

                <Form.Group as={Col}>
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    type="email"
                    name="emailId"
                    value={values.emailId}
                    onChange={handleChange}
                    isValid={touched.emailId && !errors.emailId}
                    isInvalid={touched.emailId && errors.emailId}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.emailId}
                  </Form.Control.Feedback>
                </Form.Group>
              </Form.Row>

              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    name="password"
                    value={values.password}
                    onChange={handleChange}
                    isValid={touched.password && !errors.password}
                    isInvalid={touched.password && errors.password}
                  />
                  {/* <Form.Control.Feedback type="invalid">
                    {errors.password}
                  </Form.Control.Feedback> */}
                </Form.Group>

                <Form.Group as={Col}>
                  <Form.Label>Confirm password</Form.Label>
                  <Form.Control
                    type="password"
                    name="confirmPassword"
                    value={values.confirmPassword}
                    onChange={handleChange}
                    isValid={touched.confirmPassword && !errors.confirmPassword}
                    isInvalid={touched.confirmPassword && errors.confirmPassword}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.confirmPassword}
                  </Form.Control.Feedback>
                </Form.Group>
              </Form.Row>

              <div className="d-flex mb-2">
                <Button variant="dark" type="submit" className="p-2">Submit</Button>
                {isError && showAlert("Something went wrong (check your pseudo and email)")}
              </div>
              <Link to="/signin" className="text-primary">
                Already an account? sign in
              </Link>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  </div>;
};

export default SignUp;