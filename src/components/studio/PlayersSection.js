/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

import React, { useState, useEffect } from "react";
import { Button, Form } from "react-bootstrap";
import { store } from "../../service";

const sortPlayers = function (p1, p2) {
  return p1.pseudo.localeCompare(p2.pseudo);
};

const PlayersSection = function ({ allPlayers, invitedPlayers, setInvitedPlayers }) {
  const [availablePlayers, setAvailablePlayers] = useState([]);
  const [selectedAvailablePlayer, setSelectedAvailablePlayer] = useState(null);

  const [selectedPlayers, setSelectedPlayers] = useState([]);

  useEffect(() => {
    filterAvailablePlayers();
  }, [allPlayers]);

  useEffect(() => {
    filterAvailablePlayers();
  }, [invitedPlayers]);

  const filterAvailablePlayers = function () {
    setAvailablePlayers(allPlayers.filter(ap => !invitedPlayers.map(p => p.id).includes(ap.id) && ap.id !== store.id));
  };

  const allPlayersHandleChange = function (e) {
    e.persist();
    const { value } = e.target;
    value === "-1" ? setSelectedAvailablePlayer(null) : setSelectedAvailablePlayer(JSON.parse(value));
  };

  const playersHandleChange = function (e) {
    e.persist();
    setSelectedPlayers([...e.target.selectedOptions].map(o => JSON.parse(o.value)));
  };

  const handleAdd = function (e) {
    setInvitedPlayers([...invitedPlayers, selectedAvailablePlayer].sort(sortPlayers));
    setSelectedAvailablePlayer(null);
  };

  const handleDelete = function () {
    setInvitedPlayers(invitedPlayers.filter(p => !selectedPlayers.map(sp => sp.id).includes(p.id)));
    setSelectedPlayers([]);
  };

  return <div>
    <h4>Players</h4>
    <div className="d-flex flex-row align-items-center mb-2">
      <Form.Control as="select" onChange={allPlayersHandleChange}>
        <option value="-1">Select player...</option>
        {
          availablePlayers.map(p => <option key={p.id} value={JSON.stringify(p)}>{p.pseudo}</option>)
        }
      </Form.Control>
      <Button
        className="ml-2"
        variant="warning"
        type="submit"
        onClick={handleAdd}
        disabled={selectedAvailablePlayer === null}
      >
        Add
      </Button>
      {
        selectedPlayers.length > 0 && <Button
          className="ml-2"
          variant="danger"
          type="submit"
          onClick={handleDelete}
        >
          Delete
        </Button>
      }
    </div>
    {
      invitedPlayers.length > 0 && <Form.Group>
        <Form.Control
          as="select"
          onChange={playersHandleChange}
          multiple
        >
          {
            invitedPlayers.map(p => <option key={p.id} value={JSON.stringify(p)}>{p.pseudo}</option>)
          }
        </Form.Control>
      </Form.Group>
    }
    <hr />
  </div>
};

export default PlayersSection;