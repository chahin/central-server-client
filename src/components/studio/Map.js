/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

import "mapbox-gl/dist/mapbox-gl.css";
import "@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css";

import React, { useState, useRef, useEffect, useCallback } from "react";
import ReactMapGL, { WebMercatorViewport } from "react-map-gl";
import { Alert, ButtonToolbar, Button, InputGroup, FormControl, Badge } from "react-bootstrap";
import { Editor as Draw, EditorModes as DrawModes } from "react-map-gl-draw";
import { useHistory, useLocation } from "react-router-dom";
import * as turf from "@turf/turf";
import { saveAs } from "file-saver";
import moment from "moment";

import { GeneralSection, TeamsSection, PlayersSection, ZonesSection } from ".";
import { isEqual, useForceUpdate, createItemSource, createFillLayer, createSymbolLayer } from "../../utils/utils";
import { colors, gameModes, modes, zoneTypes, itemTypes, getImage } from "../../assets/data";
import { getFeatureStyle, getEditHandleStyle } from "../../utils/draw-style";
import { api, store } from "../../service";


let INITIAL_CONFIG = {
  game_zones: null,
  restricted_zones: null,
  flags: null,
  item_spots: null,
  game_properties: null,
  invited_players: null
};
let INITIAL_GAME_PROPERTIES = {
  game_name: "",
  start_date: moment(),
  mode: gameModes.FLAG.id,
  is_public: false,
  nb_teams: 2,
  game_duration: 1400,
  minimum_time_captured: 30,
  minimum_time_spot_unavailable: 30,
  maximum_time_spot_available: 30,
  visibility_range: 5,
  activity_range: 2
};

let isTabOpened = false;
let altertTimeoutId = null;
let intervalId = null;
let allSources = [];
let generalId = 0;
let config = INITIAL_CONFIG;

const Map = function () {
  const history = useHistory();
  const location = useLocation();
  const forceUpdate = useForceUpdate();

  /***********************************************
  * Refs                                         *
  * *********************************************/
  const interactiveMap = useRef(null);
  const [draw, setDraw] = useState(null);

  /***********************************************
  * State                                        *
  * *********************************************/
  // Map options
  const [viewport, setViewport] = useState({
    width: "100%",
    height: "100%",
    latitude: 48.549939,
    longitude: 7.746513,
    minZoom: 2,
    zoom: 11,
    attributionControl: false,
  });
  const [cursorStyle, setCursorStyle] = useState("default");
  const [onItem, setOnItem] = useState(false);

  // Drag options
  const [isDragPan, setIsDragPan] = useState(true);
  const [isMouseDown, setIsMouseDown] = useState(false);
  const [currentSource, setCurrentSource] = useState(null);

  // Selected marker
  const [selectedMarker, setSelectedMarker] = useState(null);
  const [selectedVisibility, setSelectedVisibility] = useState(null);
  const [selectedActivity, setSelectedActivity] = useState(null);

  // Modes
  const [mode, setMode] = useState(modes.NONE);
  const [drawMode, setDrawMode] = useState(DrawModes.READ_ONLY);
  const [itemType, setItemType] = useState(null);
  const [zoneType, setZoneType] = useState(null);

  // Data
  const [zones, setZones] = useState([]);
  const [flags, setFlags] = useState([]);
  const [itemSpots, setItemSpots] = useState([]);
  const [gameProperties, setGameProperties] = useState(INITIAL_GAME_PROPERTIES);
  const [allPlayers, setAllPlayers] = useState([]);
  const [invitedPlayers, setInvitedPlayers] = useState([]);
  const [initialPlayers, setInitialPlayers] = useState([]);
  const [gameId, setGameId] = useState(null);

  const [vRadius, setVRadius] = useState(100);
  const [aRadius, setARadius] = useState(50);
  const [interactiveLayerIds, setInteractiveLayerIds] = useState([]);

  // Draw
  const [selectedZoneId, setSelectedZoneId] = useState(null);
  const [selectedZoneIndex, setSelectedZoneIndex] = useState(null);

  // Texts
  const [modeText, setModeText] = useState(null);

  // Alert
  const [info, setInfo] = useState(history.location.state && history.location.state.info);
  const [isError, setIsError] = useState(false);

  const [isLoadConfig, setIsLoadConfig] = useState(false);

  /***********************************************
  * Computed                                     *
  * *********************************************/
  const map = () => interactiveMap.current.getMap();
  const modeIsNONE = () => mode === modes.NONE;
  const getGameZones = () => zones.filter(z => isEqual(z.properties.zone_type, zoneTypes.GAME_ZONE));
  const getRestrictedZones = () => zones.filter(z => isEqual(z.properties.zone_type, zoneTypes.RESTRICTED_ZONE));

  /***********************************************
  * Effects                                      *
  * *********************************************/
  const drawRef = useCallback(node => {
    setDraw(node);
  }, []);

  useEffect(() => {
    return () => {
      intervalId && clearInterval(intervalId);
    }
  }, []);

  useEffect(() => {
    checkConfigImport();
  }, [draw]);

  useEffect(() => {
    if (location.pathname === "/studio") {
      if (!isTabOpened) {
        isTabOpened = true;
        getUsers();
        intervalId = setInterval(getUsers, 30000);
        checkConfigImport();
        fitToBbox();
      }
    } else {
      isTabOpened = false;
      intervalId && clearInterval(intervalId);
    }
  }, [location]);

  useEffect(() => {
    if (isLoadConfig) {
      config = location.state.config;
      if (location.state && location.state.config) {
        const state = { ...location.state };
        delete state.config;
        history.replace({ ...location, state });
      }
      setGameId(location.state.gameId);
      loadConfig();
    } else {
      fitToBbox();
    }
  }, [isLoadConfig]);

  useEffect(() => {
    if (!info) {
      if (history.location.state && history.location.state.info) {
        const state = { ...history.location.state };
        delete state.info;
        history.replace({ ...history.location, state });
      }
    }
  }, [info]);

  useEffect(() => {
    setSelectedZoneId(null);
    setSelectedZoneIndex(null);
    switch (mode) {
      case modes.ZONE:
        setItemType(null);
        if (drawMode === DrawModes.EDITING)
          setModeText("Edit zones")
        else if (drawMode === DrawModes.DRAW_POLYGON)
          setModeText("Draw a " + zoneType.name.toLowerCase());
        else
          setModeText(null);
        break;
      case modes.ITEM:
        setZoneType(null);
        setModeText("Place a " + itemType.name.toLowerCase() + " spot");
        break;
      default:
        setItemType(null);
        setZoneType(null);
        setModeText(null);
    };
  }, [mode, itemType, zoneType, drawMode]);

  useEffect(() => {
    if (!zoneType) {
      setDrawMode(DrawModes.READ_ONLY);
    }
  }, [zoneType]);

  useEffect(() => {
    checkAllItems();
    checkRestrictedZones();
    zones.length === 0 && resetModes();
  }, [zones]);

  useEffect(() => {
    if (currentSource) {
      moveMarkerToTop(currentSource);
    }
  }, [currentSource]);

  useEffect(() => {
    config = {
      game_properties: gameProperties,
      game_zones: getGameZones() || null,
      restricted_zones: getRestrictedZones() || null,
      flags: flags.length === 0 ? null : flags,
      item_spots: itemSpots.length === 0 ? null : itemSpots,
      invited_players: invitedPlayers
    };
  }, [gameProperties, zones, flags, itemSpots, invitedPlayers, isLoadConfig]);

  useEffect(() => {
    checkAllItems();
  }, [selectedMarker]);

  /***********************************************
  * Utils                                        *
  * *********************************************/
  const checkConfigImport = function () {
    if (location.state) {
      setInfo(location.state.info);
      if (!!location.state.config && draw !== null) {
        resetStudio();
      }
    }
  };

  /***********************************************
  * Error checking                               *
  * *********************************************/
  const hasItemError = function () {
    for (const s of allSources) {
      if (Object.values(s.state).includes(true))
        return true;
    }
    return false;
  };

  const hasZoneError = function () {
    for (const z of zones) {
      if (z.properties.is_error)
        return true;
    }
    return false;
  };

  const hasConfigError = function () {
    return Object.values(config).includes(null) || !config.game_properties.game_name;
  };

  const hasError = function () {
    return [hasConfigError(), hasItemError(), hasZoneError()].includes(true);
  };

  /***********************************************
  * Validators                                   *
  * *********************************************/
  const checkMode = function (m) {
    if (!Object.values(modes).includes(m)) throw new Error("Mode is not valid");
  };

  const checkItem = function (itemId) {
    if (!Object.values(itemTypes).map(i => i.id).includes(itemId)) throw new Error("Item type is not valid");
  };

  const checkZone = function (zoneId) {
    if (!Object.values(zoneTypes).map(z => z.id).includes(zoneId)) throw new Error("Zone type is not valid");
  };

  const isEmpty = function (value) {
    return value === "";
  }

  const isNumber = function (value) {
    return value !== "" && /^[0-9\b]+$/.test(value);
  };

  /***********************************************
  * Event Handlers                               *
  * *********************************************/
  /* Inputs ********************************/
  const handleVisibilityChange = function (value, setVisibility) {
    if (isEmpty(value) || isNumber(value)) {
      setVisibility(value);
    }
  };

  const handleVisibilityBlur = function (value, activity, setVisibility) {
    if (isEmpty(value) || Number(value) < Number(activity)) {
      setVisibility(activity)
    } else {
      setVisibility(value);
    }
  };

  const handleActivityChange = function (value, setActivity) {
    if (isEmpty(value) || isNumber(value)) {
      setActivity(value);
    }
  };

  const handleActivityBlur = function (value, visibility, setActivity) {
    if (isEmpty(value) || Number(value) > Number(visibility)) {
      setActivity(visibility)
    } else {
      setActivity(value);
    }
  };

  /* Buttons *******************************/
  const selectMode = function (m) {
    checkMode(m);
    setMode(m);
  };

  const editZoneMode = function () {
    setMode(modes.ZONE);
    setZoneType(zoneTypes.RESTRICTED_ZONE);
    setDrawMode(DrawModes.EDITING);
  };

  const resetModes = function () {
    setMode(modes.NONE);
    setDrawMode(DrawModes.READ_ONLY);
  };

  const selectItemType = function (item) {
    checkItem(item.id);
    selectMode(modes.ITEM);
    setItemType(item);
  };

  const selectZoneType = function (zone) {
    checkZone(zone.id);
    selectMode(modes.ZONE);
    setZoneType(zone);
    setDrawMode(DrawModes.DRAW_POLYGON);
  };

  /* Draw **********************************/
  const onSelect = (e) => {
    if (e.selectedFeature) {
      setSelectedZoneId(e.selectedFeature.properties.id);
      setSelectedZoneIndex(draw.getFeatures().indexOf(e.selectedFeature));
    } else {
      setSelectedZoneIndex(null);
    }
  };

  const onUpdate = (e) => {
    const features = draw.getFeatures();
    if (e.editType === "addFeature") {
      let newFeature = turf.cleanCoords(features[features.length - 1]);
      newFeature.properties.zone_type = zoneType;
      resetModes();
    }
    setZones(features);
  };

  /* Map ***********************************/
  const onLoad = function ({ target: m }) {
    Object.values(itemTypes).forEach(item => {
      const link = "https://s3.princelle.org/konflict/" + item.img;
      m.loadImage(
        link,
        function (error, image) {
          if (error) throw error;
          m.addImage(item.id, image);
        }
      );
    });
  };

  const onClick = function (e) {
    unselectMarker();
    if (mode === modes.ITEM) {
      createMarker(itemType, e.lngLat, vRadius, aRadius);
      checkAllItems();
    }
    if (modeIsNONE() && onItem) {
      selectMarker(getMarkersAtPoint(e.lngLat)[0]);
    }
  };

  const onMouseEnter = function (e) {
    setOnItem(true);
    setCursorStyle("pointer");
  };

  const onMouseLeave = function (e) {
    setOnItem(false);
    setCursorStyle("default");
  };

  const onMouseMove = function (e) {
    //dragging
    if (isMouseDown) {
      cursorStyle !== "grabbing" && setCursorStyle("grabbing");
      if (modeIsNONE() && currentSource) {
        editCurrentMarkerLongLat(currentSource, e.lngLat);
      }
    }
  };

  const onMouseDown = function (e) {
    if (modeIsNONE()) {
      const sourceAtPoint = getMarkersAtPoint(e.lngLat)[0];
      sourceAtPoint && setIsDragPan(false);
      setCurrentSource(sourceAtPoint);
    }
    setIsMouseDown(true);
  };

  const onMouseUp = function (e) {
    setIsMouseDown(false);
    setIsDragPan(true);
    checkAllItems();
    if (modeIsNONE()) {
      setCurrentSource(null);
    }
    if (currentSource) {
      let elements;
      let setElements;
      if (isEqual(currentSource.type, itemTypes.FLAG)) {
        elements = flags;
        setElements = setFlags;
      } else {
        elements = itemSpots;
        setElements = setItemSpots;
      }
      const otherElements = elements.filter(e => Number(e.id) !== Number(currentSource.id));
      const element = elements.find(e => Number(e.id) === Number(currentSource.id));
      setElements([...otherElements, element]);
    }
    setCursorStyle("default");
  };

  /***********************************************
  * Methods                                      *
  * *********************************************/
  const getUsers = function () {
    api.users().then(res => {
      const { data: users } = res;
      setAllPlayers(users.map(u => {
        return {
          id: u.id,
          pseudo: u.pseudo
        }
      }));
    });
  };

  const saveConfig = function () {
    const { game_name, start_date, is_public, game_duration } = config.game_properties;
    const game = {
      name: game_name,
      dateStart: start_date.format("YYYY-MM-DD hh:mm:ss"),
      is_public: is_public,
      creator: {
        id: store.id
      },
      duration: game_duration,
      data: JSON.stringify(config)
    };

    const onSuccess = function ({ data: game }) {
      let promises = [];
      if (gameId) {
        const playersToRemove = initialPlayers.filter(p1 => !invitedPlayers.map(p2 => p2.id).includes(p1.id))
        playersToRemove.forEach(p => {
          promises.push(api.rmUserInviteOfGame(p.id, game.id));
        });
      }

      invitedPlayers.forEach(p => {
        promises.push(api.inviteUserToGame(p.id, game.id));
      });
      Promise.all(promises).then(values => console.log(values));
      setInfo({
        type: "success",
        message: "Game successfully saved"
      });
    };

    const onError = function (err) {
      console.error(err);
      setInfo({
        type: "danger",
        message: "An error occured"
      });
    };

    gameId
      ? api.editGame(gameId, game)
        .then(onSuccess)
        .catch(onError)
      : api.createGame(game)
        .then(onSuccess)
        .catch(onError)
  };

  const loadConfig = function () {
    const featuresToAdd = [...config.game_zones, ...config.restricted_zones];
    draw.addFeatures(featuresToAdd);
    setZones(featuresToAdd);

    const itemsToAdd = [...config.flags, ...config.item_spots];
    const flgs = [];
    const itms = []
    const ilids = [];
    itemsToAdd.forEach(i => {
      const { item, newInteractiveLayerIds } = createMarker(i.type, i.localisation, i.visibility_range, i.activity_range);
      isEqual(i.type, itemTypes.FLAG)
        ? flgs.push(item)
        : itms.push(item);
      ilids.push(...newInteractiveLayerIds);
    });
    setFlags([...flags, ...flgs])
    setItemSpots([...itemSpots, ...itms])
    setInteractiveLayerIds([...interactiveLayerIds, ...ilids]);
    setGameProperties({
      ...config.game_properties,
      start_date: moment(config.game_properties.start_date, "YYYY-MM-DD hh:mm:ss")
    });
    setInitialPlayers(config.invited_players);
    setInvitedPlayers(config.invited_players);
    setIsLoadConfig(false);
  };

  const resetStudio = function () {
    deleteAllItems();
    deleteAllZones();
    resetModes();
    setIsDragPan(true);
    setIsMouseDown(false);
    setCurrentSource(null);
    unselectMarker();
    setAllPlayers([]);
    setInitialPlayers([]);
    setInvitedPlayers([]);
    setModeText(null);
    setVRadius(100);
    setARadius(50);
    setGameProperties(INITIAL_GAME_PROPERTIES);
    setIsLoadConfig(true);
    allSources = [];
    generalId = 0;
    config = INITIAL_CONFIG;
  }

  const deleteAllZones = function () {
    if (draw) {
      let all = [];
      zones.forEach((z, i) => all.push(i));
      draw.deleteFeatures(all);
    }
    setZones([]);
  };

  const deleteAllItems = function () {
    setInteractiveLayerIds([]);
    allSources.forEach(s => {
      Object.values(s.layers).map(l => l.id).forEach(id => map().removeLayer(id));
      map().removeSource(s.id)
    });
    allSources = [];
    setItemSpots([]);
    setFlags([]);
  };

  /* Markers *******************************/
  const createMarker = function (type, localisation, visibility, activity) {
    if (isNaN(activity) || isNaN(visibility)) throw new Error("Both activity and visibility radius have to be of type number");

    activity = Number(activity);
    visibility = Number(visibility);
    if (activity > visibility) throw new Error("Activity radius can't be bigger than visibility one");

    const id = generalId.toString();
    generalId++;
    const ids = {
      "A": `${id}-activity`,
      "V": `${id}-visibility`,
      "P": `${id}-point`
    };

    const source = createItemSource(localisation, visibility, activity);

    const activityLayer = createFillLayer(ids["A"], id, colors.ACTIVITY_LAYER, "activity");
    const visibilityLayer = createFillLayer(ids["V"], id, colors.VISIBILITY_LAYER, "visibility");
    const pointLayer = createSymbolLayer(ids["P"], id, type.id);

    map().addSource(id, source);

    map().addLayer(visibilityLayer);
    map().addLayer(activityLayer);
    map().addLayer(pointLayer);

    const item = {
      id: Number(id),
      type,
      localisation,
      visibility_range: visibility,
      activity_range: activity
    };

    allSources.push({
      id: Number(id),
      type,
      mapSource: map().getSource(id),
      data: {
        center: localisation,
        visibility,
        activity
      },
      state: {
        isError: false,
        isOverlap: false
      },
      layers: {
        visibility: visibilityLayer,
        activity: activityLayer,
        point: pointLayer
      }
    });
    setInteractiveLayerIds([...interactiveLayerIds, ...Object.values(ids)]);
    isEqual(type, itemTypes.FLAG)
      ? setFlags([...flags, item])
      : setItemSpots([...itemSpots, item]);

    for (const key in ids) {
      if (key === "P") {
        map().setPaintProperty(
          ids[key],
          "icon-opacity",
          ["interpolate", ["exponential", 0.5], ["zoom"], 4, 0, 15, 1]
        );
        map().setLayoutProperty(
          ids[key],
          "icon-size",
          // ["interpolate", ["exponential", 1], ["zoom"], 3, 0, 15, 0.7]
          ["interpolate", ["exponential", 1], ["zoom"], 0, 0, 15, 0.1]
        );
      } else {
        // map().setPaintProperty(
        //   ids[key], 
        //   "fill-opacity",
        //   ["interpolate", ["exponential", 0.5], ["zoom"], 4, 0, 10, 0.5]
        // );
      }
    };
    return { newInteractiveLayerIds: Object.values(ids), item };
  }

  const selectMarker = function (markerSource) {
    setSelectedVisibility(markerSource.data.visibility);
    setSelectedActivity(markerSource.data.activity);
    setSelectedMarker(markerSource);
    map().setPaintProperty(markerSource.layers.visibility.id, "fill-color", colors.SELECTED);
    map().setPaintProperty(markerSource.layers.activity.id, "fill-color", colors.SELECTED);
  };

  const unselectMarker = function () {
    if (selectedMarker) {
      map().setPaintProperty(selectedMarker.layers.visibility.id, "fill-color", colors.VISIBILITY_LAYER);
      map().setPaintProperty(selectedMarker.layers.activity.id, "fill-color", colors.ACTIVITY_LAYER);
      setSelectedMarker(null);
      setSelectedVisibility(null);
      setSelectedActivity(null);
    }
  };

  // gets the marker sources at point
  const getMarkersAtPoint = function (point) {
    const sources = [];
    allSources.forEach(source => {
      if (turf.booleanWithin(turf.point(point), source.mapSource._data.features[0])) {
        sources.push(source);
      }
    });
    return sources;
  };

  // edit the current marker source
  const editCurrentMarkerLongLat = function (markerSource, localisation) {
    const index = allSources.indexOf(allSources.find(e => e.id === currentSource.id))
    allSources[index] = markerSource;

    let elements = isEqual(markerSource.type, itemTypes.FLAG) ? flags : itemSpots;
    const element = elements.find(e => Number(e.id) === Number(markerSource.id));
    element.localisation = localisation;
    markerSource.mapSource.setData(createItemSource(localisation, Number(element.visibility_range), Number(element.activity_range)).data);
    markerSource.data.center = localisation;
    return element;
  };

  const editMarker = function (markerSource, localisation, visibility, activity) {
    const index = allSources.indexOf(allSources.find(e => e.id === selectedMarker.id))
    allSources[index] = markerSource;

    let elements = isEqual(markerSource.type, itemTypes.FLAG) ? flags : itemSpots;
    const element = elements.find(e => Number(e.id) === Number(markerSource.id));
    element.localisation = localisation || element.localisation;
    element.visibility_range = Number(visibility || element.visibility_range);
    element.activity_range = Number(activity || element.activity_range);
    markerSource.mapSource.setData(createItemSource(element.localisation, element.visibility_range, element.activity_range).data);
    markerSource.data.localisation = element.localisation;
    markerSource.data.visibility = element.visibility_range;
    markerSource.data.activity = element.activity_range;
  }

  const moveMarkerToTop = function (source) {
    Object.values(source.layers).forEach(l => map().moveLayer(l.id));
    // also move sources at the beginning so getSourcesAtPoint function
    // gets the right layer if multiple are found at the same point.
    allSources.splice(0, 0, allSources.splice(allSources.indexOf(source), 1)[0]);
  };

  const removeMarker = function (markerSource) {
    const layerIds = Object.values(markerSource.layers).map(l => l.id);
    layerIds.forEach(id => map().removeLayer(id));
    setInteractiveLayerIds(interactiveLayerIds.filter(l => !layerIds.includes(l)))
    map().removeSource(markerSource.id);
    setCurrentSource(null);
    setSelectedMarker(null);
    const index = allSources.indexOf(markerSource);
    index > -1 && allSources.splice(index, 1);

    const idxFlags = flags.indexOf(flags.find(f => f.id === markerSource.id));
    if (idxFlags > -1) {
      flags.splice(idxFlags, 1)
      setFlags(flags);
    }

    const idxItems = itemSpots.indexOf(itemSpots.find(i => i.id === markerSource.id));
    if (idxItems > -1) {
      itemSpots.splice(idxItems, 1)
      setItemSpots(itemSpots);
    }
  };

  const deleteSelectedZone = function () {
    setZones(zones.filter(z => z.properties.id !== selectedZoneId));
    draw.deleteFeatures(selectedZoneIndex);
    setSelectedZoneIndex(null);
  };

  /* Checking *******************************/
  const checkRestrictedZones = function () {
    const restrictedZones = getRestrictedZones();
    const gameZones = getGameZones();
    for (const rz of restrictedZones) {
      rz.properties.is_error = true;
      for (const gz of gameZones) {
        if (turf.booleanWithin(rz, gz)) {
          rz.properties.is_error = false;
          break;
        }
      }
    }
    forceUpdate();
  };

  const checkAllItems = function () {
    checkItemsInZone();
    checkFlagsOverlap();
  }

  const checkFlagsOverlap = function () {
    const flagSources = allSources.filter(s => isEqual(s.type, itemTypes.FLAG));
    if (flagSources.length >= 2) {
      const overlapped = []
      for (const f1 of flagSources) {
        for (const f2 of flagSources) {
          if (f1.id !== f2.id) {
            const af1 = f1.mapSource._data.features[1]; // activity feature 1
            const af2 = f2.mapSource._data.features[1];
            const { id: al1 } = f1.layers.activity; // activity layer 1
            const { id: al2 } = f2.layers.activity;
            if (turf.booleanOverlap(af1, af2) || turf.booleanWithin(af1, af2)) {
              overlapped.includes(al1) || (selectedMarker && f1.id === selectedMarker.id) || overlapped.push(f1);
              overlapped.includes(al2) || (selectedMarker && f2.id === selectedMarker.id) || overlapped.push(f2);
            } else {
              !f1.state.isError && map().setPaintProperty(
                al1,
                "fill-color",
                (selectedMarker && f1.id === selectedMarker.id)
                  ? colors.SELECTED
                  : colors.ACTIVITY_LAYER
              );
              !f2.state.isError && map().setPaintProperty(
                al2,
                "fill-color",
                (selectedMarker && f2.id === selectedMarker.id)
                  ? colors.SELECTED
                  : colors.ACTIVITY_LAYER
              );
              f1.state.isOverlap = false;
              f2.state.isOverlap = false;
            }
          }
        }
      }
      overlapped.forEach(f => {
        map().setPaintProperty(f.layers.activity.id, "fill-color", colors.ERROR);
        f.state.isOverlap = true;
      })
      forceUpdate();
    }
  };

  const checkItemsInZone = function () {
    if (allSources.length >= 1) {
      const gzs = getGameZones();
      const rzs = getRestrictedZones();
      for (const item of allSources) {
        const { id: visibilityLayer } = item.layers.visibility;
        const { id: activityLayer } = item.layers.activity;
        const center = turf.point(item.data.center); // center the item

        const changeColorToRed = function () {
          map().setPaintProperty(
            visibilityLayer,
            "fill-color",
            (selectedMarker && item.id === selectedMarker.id)
              ? colors.SELECTED
              : colors.ERROR
          );
          map().setPaintProperty(
            activityLayer,
            "fill-color",
            (selectedMarker && item.id === selectedMarker.id)
              ? colors.SELECTED
              : colors.ERROR
          );
        };

        const resetColor = function () {
          map().setPaintProperty(
            visibilityLayer,
            "fill-color",
            (selectedMarker && item.id === selectedMarker.id)
              ? colors.SELECTED
              : colors.VISIBILITY_LAYER
          );
          map().setPaintProperty(
            activityLayer,
            "fill-color",
            (selectedMarker && item.id === selectedMarker.id)
              ? colors.SELECTED
              : colors.ACTIVITY_LAYER
          );
        };

        let rzError = false;

        if (rzs.length > 0) {
          for (const rz of rzs) {
            if (turf.booleanPointInPolygon(center, rz)) {
              rzError = true;
              break;
            }
            rzError = false;
          }
        }

        let gzErrors = [];

        if (gzs.length > 0) {
          for (const gz of gzs) {
            if (!turf.booleanPointInPolygon(center, gz)) {
              gzErrors.push(true);
            } else {
              gzErrors.push(false);
            }
          }
        } else {
          gzErrors.push(true);
        }

        const isError = !gzErrors.includes(false) || rzError;
        isError ? changeColorToRed() : resetColor();
        item.state.isError = isError;
      }
    }
    forceUpdate();
  };

  const downloadConfig = function () {
    // if (isError()) {
    // alert("Please fix errors to export");
    // } else {
    saveAs(new Blob([JSON.stringify(config, null, 2)], { type: "application/json" }), "config.json");
    // }
  };

  /* Teams ****************************/
  const createTeam = function () {
    setGameProperties({
      ...gameProperties,
      nb_teams: gameProperties.nb_teams + 1
    });
  };

  const deleteTeam = function () {
    setGameProperties({
      ...gameProperties,
      nb_teams: gameProperties.nb_teams - 1
    });
  };

  const fitToBbox = function () {
    const features = [...zones];
    allSources.forEach(s => features.push(...s.mapSource._data.features));
    if (features.length > 0) {
      const [minLng, minLat, maxLng, maxLat] = turf.bbox({
        type: "FeatureCollection",
        features
      });
      const vp = new WebMercatorViewport(viewport);
      const { longitude, latitude, zoom } = vp.fitBounds([[minLng, minLat], [maxLng, maxLat]], {
        padding: 100
      });

      setViewport({
        ...viewport,
        longitude,
        latitude,
        zoom
      });
    }
  };

  const testfunc = function () {
    console.log(config);
  };

  /***********************************************
  * Render                                       *
  * *********************************************/
  const showError = function () {
    return showAlert("An error occured", "danger", setIsError)
  };

  const showInfo = function () {
    return showAlert(info.message, info.type, setInfo);
  };

  const showAlert = function (message, variant, setValue) {
    altertTimeoutId && clearTimeout(altertTimeoutId);
    altertTimeoutId = setTimeout(() => {
      setValue(false);
    }, 5000);
    return <Alert className="m-0 ml-3 p-2" variant={variant}>
      {message}
    </Alert>
  };

  const editionPanel = function () {
    return <div className="bg-white mb-4 p-3 overflow-auto h-100" style={{ width: "100%" }}>
      <div>
        <h4>Edit item</h4>
        <InputGroup className="mt-2">
          <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon3">
              Visibility (in m)
          </InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            value={selectedVisibility}
            onChange={({ target: { value } }) => handleVisibilityChange(value, setSelectedVisibility)}
            onBlur={({ target: { value } }) => handleVisibilityBlur(value, selectedActivity, setSelectedVisibility)}
          />
        </InputGroup>
        <InputGroup className="mt-2">
          <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon3">
              Activity (in m)
            </InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            value={selectedActivity}
            onChange={({ target: { value } }) => handleActivityChange(value, setSelectedActivity)}
            onBlur={({ target: { value } }) => handleActivityBlur(value, selectedVisibility, setSelectedActivity)}
          />
        </InputGroup>
        <Button className="mr-2 mt-2" variant="warning" onClick={() => removeMarker(selectedMarker)}>Delete item</Button>
        <Button className="mr-2 mt-2" variant="warning" onClick={() => {
          editMarker(selectedMarker, null, selectedVisibility, selectedActivity);
          isEqual(selectedMarker.type, itemTypes.FLAG) && checkAllItems();
        }}>Submit</Button>
      </div>
    </div>
  };

  const creationPanel = function () {
    return <div
      className={`bg-white mb-4 p-3 overflow-auto h-100${selectedMarker ? " d-none" : ""}`}
      style={{ width: "320px" }}
    >
      <GeneralSection gameProperties={gameProperties} onChange={setGameProperties} />
      {gameProperties.is_public || <PlayersSection allPlayers={allPlayers} invitedPlayers={invitedPlayers} setInvitedPlayers={setInvitedPlayers} />}
      <TeamsSection onCreate={createTeam} onDelete={deleteTeam} nbTeams={gameProperties.nb_teams} />
      <ZonesSection
        onChange={selectZoneType}
        onEdit={editZoneMode}
        onDelete={deleteSelectedZone}
        zoneSelected={selectedZoneIndex !== null}
        zonesAvailable={zones && zones.length > 0}
      />
      <div>
        <h4>Items</h4>
        <ButtonToolbar>
          {
            Object.values(itemTypes).map(item =>
              <Button
                className="mr-2 mb-2"
                variant="warning"
                key={item.id}
                onClick={(e) => selectItemType(item)}
              >
                <img style={{ height: "20px" }} src={getImage(item.img)} alt="" />
                {"   " + item.name}
              </Button>
            )
          }
        </ButtonToolbar>
        <InputGroup className="mb-2">
          <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon3">
              Visibility (in m)
          </InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            value={vRadius}
            onChange={({ target: { value } }) => handleVisibilityChange(value, setVRadius)}
            onBlur={({ target: { value } }) => handleVisibilityBlur(value, aRadius, setVRadius)}
          />
        </InputGroup>
        <InputGroup>
          <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon3">
              Activity (in m)
          </InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            value={aRadius}
            onChange={({ target: { value } }) => handleActivityChange(value, setARadius)}
            onBlur={({ target: { value } }) => handleActivityBlur(value, vRadius, setARadius)}
          />
        </InputGroup>
      </div>
      <hr />
      <div>
        <h4>Actions</h4>
        <ButtonToolbar>
          {/* <Button
            className="mr-2 mt-2"
            variant="warning"
            onClick={downloadConfig}
            disabled={hasError()}
          >
            Export config
        </Button> */}
          <Button
            className="mr-2 w-100"
            variant="warning"
            onClick={saveConfig}
            disabled={hasError()}
          >
            Save game
        </Button>
        </ButtonToolbar>
      </div>
      {/* <hr/>
    <Button className="mr-2 mt-2" variant="warning" onClick={cleanup}>Finish</Button> */}
    </div >
  };

  return <>
    <div className="d-flex h-100">
      <div className="h-100 flex-grow-1 p-3 d-flex flex-column">
        <div className="d-flex justify-content-between align-items-stretch mb-3">
          <div className="d-flex align-items-center">
            <h4 className="font-weight-semi-bold m-0 py-2">Studio</h4>
            {gameId && <Badge className="ml-2" variant="warning">imported</Badge>}
            {isError ? showError() : info && showInfo()}
          </div>
          {/* <Button className="mr-2 mt-2" variant="success" onClick={testfunc}>TEST</Button> */}
          {
            modeText && <div className="d-flex">
              <div className="bg-white rounded p-2 d-flex align-items-center">
                {modeText}
              </div>
              <Button
                variant="dark"
                type="submit"
                className="ml-2"
                onClick={resetModes}
              >
                Done
              </Button>
            </div>
          }
        </div>
        <div className="flex-grow-1">
          <ReactMapGL
            {...viewport}
            onViewportChange={(data) => {
              setViewport({
                ...data,
                width: data.width || "100%",
                height: data.height || "100%"
              })
            }}
            mapboxApiAccessToken="pk.eyJ1IjoiY2hhaGluIiwiYSI6ImNrNjViOWJ1ODE1MXEzbXExMmM0bGFic2YifQ.UokERyQ0HI2FDYeaE64zPg"
            mapStyle="mapbox://styles/chahin/ck65nir2h0vrn1inpj04by95g?optimize=true"
            className="rounded-extra"
            ref={interactiveMap}
            onNativeClick={onClick}
            onLoad={onLoad}
            interactiveLayerIds={interactiveLayerIds}
            doubleClickZoom={false}
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
            onMouseMove={onMouseMove}
            onMouseDown={onMouseDown}
            onMouseUp={onMouseUp}
            getCursor={() => cursorStyle}
            dragPan={isDragPan}
          >
            {/* <Popup
                latitude={37.78}
                longitude={-122.41}
                closeButton={true}
                closeOnClick={false}
                // onClose={() => this.setState({showPopup: false})}
                anchor="top" >
                <div>You are here</div>
              </Popup> */}

            <Draw
              ref={drawRef}
              style={{ width: "100%", height: "100%" }}
              clickRadius={12}
              mode={drawMode}
              onUpdate={onUpdate}
              onSelect={onSelect}
              editHandleShape={"circle"}
              editHandleStyle={(e) => getEditHandleStyle(e, zoneType)}
              featureStyle={(e) => getFeatureStyle(e, zoneType)}
            />
          </ReactMapGL>
        </div>
      </div>
      <div>
        {selectedMarker ? editionPanel(selectedMarker) : creationPanel()}
      </div>
    </div>
  </>;
};

export default Map;