/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

import React from "react";
import { ButtonToolbar, Button } from "react-bootstrap";
import { zoneTypes } from "../../assets/data";

const ZonesSection = function ({ onChange, onEdit, onDelete, zoneSelected, zonesAvailable }) {
  return <div>
    <h4>Zones</h4>
    <ButtonToolbar>
      {
        Object.values(zoneTypes).map(zone =>
          <Button
            className="mr-2"
            variant="warning"
            key={zone.id}
            onClick={(e) => onChange(zone)}
          >
            {zone.name}
          </Button>
        )
      }
    </ButtonToolbar>
    {
      zonesAvailable && <Button
        className="mr-2 mt-2"
        variant="warning"
        onClick={onEdit}
      >
        <b>Edit zones</b>
      </Button>
    }
    {
      zoneSelected && <Button
        className="mr-2 mt-2"
        variant="danger"
        onClick={onDelete}
      >
        Delete zone
      </Button>
    }
    <hr />
  </div>;
};

export default ZonesSection;