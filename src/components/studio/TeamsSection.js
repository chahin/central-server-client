/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

import React, { useState } from "react";
import { Badge, Button, Collapse } from "react-bootstrap";

const TeamsSection = function ({ onCreate, onDelete, nbTeams }) {
  const [open, setOpen] = useState(false);

  return <div>
    <div className="d-flex align-items-center">
      <h4 className="d-inline m-0 mr-2">Teams</h4>
      <Button
        className="mr-2"
        variant="warning"
        onClick={() => setOpen(!open)}
        aria-expanded={open}
      >
        {open ? "hide " : "show "}
        <Badge variant="light">{nbTeams}</Badge>
      </Button>
    </div>
    <div>
      <Collapse in={open} className="mt-2">
        <div>
          <Button
            className="mt-2 mr-2"
            variant="warning"
            onClick={onCreate}
          >
            New Team
          </Button>
          {
            nbTeams > 2 && <Button
              className="mt-2"
              variant="danger"
              onClick={onDelete}
            >
              Delete Team
          </Button>
          }
          {
            (() => {
              const res = [];
              for (let i = 1; i < nbTeams + 1; i++) {
                res.push(<div className="bg-light d-flex justify-content-between align-items-center mt-2 p-2 border rounded" key={i}>
                  <p className="m-0">Team {i}</p>
                </div>);
              }
              return res;
            })()
          }
        </div>
      </Collapse>
    </div>
    <hr />
  </div>
};

export default TeamsSection;