/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

import React, { useState } from "react";
import { Button, Form, FormControl, Collapse, ToggleButtonGroup, ToggleButton, InputGroup } from "react-bootstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";

import { gameModes } from "../../assets/data";

const GeneralSection = function ({ gameProperties, onChange }) {
  const [open, setOpen] = useState(false);

  return <div>
    <h4>General</h4>
    <h6>Name</h6>
    <FormControl
      type="text"
      value={gameProperties.game_name}
      onChange={({ target }) => onChange({ ...gameProperties, game_name: target.value })} />
    <h6 className="mt-2">Start date</h6>
    <DatePicker
      className="p-2 rounded form-control"
      selected={gameProperties.start_date._d}
      onChange={d => onChange({ ...gameProperties, start_date: moment(d) })}
      timeInputLabel="Time:"
      showTimeInput
    />
    <h6 className="mt-2">Mode</h6>
    <Form.Group controlId="modeSelect">
      <Form.Control
        as="select"
        value={gameProperties.mode}
        onChange={(e) => { e.persist(); onChange({ ...gameProperties, mode: Number(e.target.value) }) }}
      >
        {
          Object.values(gameModes).map(g => {
            return <option
              key={g.id}
              value={g.id}
            >
              {g.name}
            </option>
          })
        };
          </Form.Control>
    </Form.Group>

    <div className="d-flex align-items-center">
      <h6 className="d-inline m-0 mr-2">Advanced options</h6>
      <Button
        className="mr-2"
        variant="warning"
        size="sm"
        onClick={() => setOpen(!open)}
        aria-expanded={open}
      >
        {open ? "hide " : "show "}
      </Button>
    </div>
    <Collapse in={open} className="mt-2 w-100">
      <div>
        <ToggleButtonGroup
          type="radio"
          name="options"
          defaultValue={gameProperties.is_public}
          onChange={(value) => onChange({ ...gameProperties, is_public: value })}
        >
          <ToggleButton value={false} variant="light">Private</ToggleButton>
          <ToggleButton value={true} variant="light">Public</ToggleButton>
        </ToggleButtonGroup>
        <InputGroup className="mt-2">
          <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon3">
              Game duration (in s)
              </InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            value={gameProperties.game_duration}
            onChange={({ target }) => onChange({ ...gameProperties, game_duration: target.value || 0 })}
          />
        </InputGroup>
        <InputGroup className="mt-2">
          <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon3">
              Flag limit time (in s)
              </InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            value={gameProperties.minimum_time_captured}
            onChange={({ target }) => onChange({ ...gameProperties, minimum_time_captured: target.value || 0 })}
          />
        </InputGroup>
        <InputGroup className="mt-2">
          <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon3">
              Max available spot time (in s)
              </InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            value={gameProperties.maximum_time_spot_available}
            onChange={({ target }) => onChange({ ...gameProperties, maximum_time_spot_available: target.value || 0 })}
          />
        </InputGroup>
        <InputGroup className="mt-2">
          <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon3">
              Min unavailable spot time (in s)
              </InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            value={gameProperties.minimum_time_spot_unavailable}
            onChange={({ target }) => onChange({ ...gameProperties, minimum_time_spot_unavailable: target.value || 0 })}
          />
        </InputGroup>
        <InputGroup className="mt-2">
          <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon3">
              Players visibility range (in m)
              </InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            value={gameProperties.visibility_range}
            onChange={({ target }) => onChange({ ...gameProperties, visibility_range: target.value || 0 })}
          />
        </InputGroup>
        <InputGroup className="mt-2">
          <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon3">
              Players activity range (in m)
              </InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            value={gameProperties.activity_range}
            onChange={({ target }) => onChange({ ...gameProperties, activity_range: target.value || 0 })}
          />
        </InputGroup>
      </div>
    </Collapse>
    <hr />
  </div>
};

export default GeneralSection;