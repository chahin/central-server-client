/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

import React from "react";
import { Button } from "react-bootstrap";
import { getImage } from "../assets/data";
import { Link } from "react-router-dom";

const Welcome = function () {
  return <div className="vh-100 bg-black overflow-scroll" style={{
    backgroundImage: `url(${getImage("noise.png")}), url(${getImage("konflict.png")}), url(${getImage("cocainMap.png")})`,
    backgroundPosition: "top left, top left, bottom right",
    backgroundRepeat: "repeat, no-repeat, no-repeat",
    backgroundSize: "cover, 30%, 30%"
  }}>
    <div className="h-100 w-100 d-flex flex-column align-items-center justify-content-center text-white">
      <h3 className="mb-4 font-italic">Become the coolest narcotic now.</h3>
      <Link to="/signup">
        <Button variant="light" className="font-weight-semi-bold mb-4">Join</Button>
      </Link>
      <Link to="/signin">
        Already an account? sign in
      </Link>
    </div>
  </div>;
};

export default Welcome;