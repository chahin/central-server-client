/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { Button, Card, Badge, OverlayTrigger, Tooltip } from "react-bootstrap";
import { api, store } from "../service";
import moment from "moment";

let intervalId = null;

const Home = function () {
  const history = useHistory();
  const location = useLocation();
  const [publicGames, setPublicGames] = useState([]);
  const [myGames, setMyGames] = useState([]);

  useEffect(() => {
    return () => {
      intervalId && clearInterval(intervalId);
    }
  }, []);

  useEffect(() => {
    if (location.pathname === "/home") {
      getGames();
      intervalId = setInterval(getGames, 30000)
    } else {
      intervalId && clearInterval(intervalId);
    }
  }, [location]);


  const getGames = function () {
    api.games().then(({ data: games }) => {
      if (games && games.length > 0) {
        games = games.filter(g => g.creator.id === store.id);
        games.sort((g1, g2) => {
          const d1 = moment(g1.dateCreation, "YYYY-MM-DD hh:mm:ss");
          const d2 = moment(g2.dateCreation, "YYYY-MM-DD hh:mm:ss");
          if (d1 > d2)
            return -1;
          else if (d1 < d2)
            return 1;
          return 0;
        });
        setMyGames(games.filter(g => g.creator.id === store.id));
      }
    });
  };

  const handleEdit = function (game) {
    history.push("/studio", {
      gameId: game.id,
      config: JSON.parse(game.data),
      info: {
        type: "success",
        message: "Game configuration successfully imported"
      }
    });
    // history.push()
  };

  const handleDelete = function (game) {
    api.deleteGame(game.id).then((res) => {
      setMyGames(myGames.filter(g => g.id !== game.id));
    });
  };

  return <div className="h-100 p-3 d-flex flex-column">
    <h4 className="font-weight-semi-bold m-0 py-2 mb-3">Home</h4>
    <div className="flex-grow-1 overflow-hidden">
      <div className="p-4 bg-white rounded-extra w-50 mh-100 overflow-auto">
        {
          (myGames.length === 0 && publicGames.length === 0) && <h5 className="font-weight-semi-bold m-0">No games found</h5>
        }
        {
          myGames.length > 0 && <div>
            <h5 className="font-weight-semi-bold mb-3">My Games</h5>
            {
              myGames.map(g => {
                return <Card key={g.id} className="mt-2">
                  <Card.Header as="h5">
                    {g.name}
                    {
                      g.ispublic
                        ? <Badge className="ml-2" variant="success">public</Badge>
                        : <Badge className="ml-2" variant="danger">private</Badge>
                    }
                  </Card.Header>
                  <Card.Body>
                    <Card.Text>
                      Edited {moment(g.dateCreation, "YYYY-MM-DD hh:mm:ss").calendar()}
                    </Card.Text>
                    <Button
                      variant="warning"
                      className="font-weight-semi-bold"
                      onClick={(e) => handleEdit(g)}
                    >
                      Edit in studio
                    </Button>
                    <Button
                      variant="danger"
                      className="font-weight-semi-bold ml-2"
                      onClick={(e) => handleDelete(g)}
                    >
                      Delete
                    </Button>
                  </Card.Body>
                </Card>
              })
            }
          </div>
        }
        {
          publicGames.length > 0 && <div className="mt-4">
            <h5 className="font-weight-semi-bold">Public games</h5>
            {
              publicGames.map(g => {
                return <Card key={g.id} className="mt-2">
                  <Card.Header as="h5">
                    {g.name}
                    <Badge className="ml-2" variant="success">public</Badge>
                  </Card.Header>
                  <Card.Body>
                    <Card.Text>
                      Created {moment(g.dateCreation, "YYYY-MM-DD hh:mm:ss").calendar()} by {g.creator.name}
                    </Card.Text>
                    <Button variant="warning" className="font-weight-semi-bold">Edit in studio</Button>
                  </Card.Body>
                </Card>
              })
            }
          </div>
        }
      </div>
    </div>
  </div>;
};

export default Home;