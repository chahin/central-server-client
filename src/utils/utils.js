/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

import { useState, useCallback } from "react";

const geojsonFeature = function (type, coordinates) {
  return {
    "type": "geojson",
    "data": {
      "type": "Feature",
      "geometry": {
        type,
        coordinates
      }
    }
  }
};

// radius in km
const getCirclePoints = function (localisation, radiusInM) {
  const radiusInKm = radiusInM / 1000;
  const points = 50;
  const [lng, lat] = localisation;

  let ret = [];
  let distanceX = radiusInKm / (111.320 * Math.cos(lat * Math.PI / 180));
  let distanceY = radiusInKm / 110.574;

  let theta, x, y;
  for (let i = 0; i < points; i++) {
    theta = (i / points) * (2 * Math.PI);
    x = distanceX * Math.cos(theta);
    y = distanceY * Math.sin(theta);
    ret.push([lng + x, lat + y]);
  }
  ret.push(ret[0]);

  return ret;
};

const createPointSource = function (localisation) {
  return geojsonFeature("Point", localisation);
};

const createCircleSource = function (localisation, radius) {
  const points = getCirclePoints(localisation, radius);
  return geojsonFeature("Polygon", [points]);
};

const createFillLayer = function (id, source, color, role) {
  return {
    id,
    "type": "fill",
    source,
    "layout": {},
    "paint": {
      "fill-color": color,
      "fill-opacity": 0.4,
      "fill-antialias": true,
    },
    "filter": ["==", "role", role]
  };
};

const createSymbolLayer = function (id, source, symbolId) {
  return {
    id,
    "type": "symbol",
    source,
    "layout": {
      "icon-image": symbolId,
      "icon-allow-overlap": true,
      "icon-ignore-placement": true,
      "icon-size": 0.5
    },
    "paint": {
      "icon-opacity": 0.5
    },
    "filter": ["==", "role", "point"]
  };
};

const createItemSource = function (localisation, visibility, activity) {
  return {
    "type": "geojson",
    "data": {
      "type": "FeatureCollection",
      "features": [
        {
          "type": "Feature",
          "geometry": {
            "type": "Polygon",
            "coordinates": [getCirclePoints(localisation, visibility)]
          },
          "properties": {
            "role": "visibility",
            "radius": visibility
          }
        },
        {
          "type": "Feature",
          "geometry": {
            "type": "Polygon",
            "coordinates": [getCirclePoints(localisation, activity)]
          },
          "properties": {
            "role": "activity",
            "radius": activity
          }
        },
        {
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": localisation
          },
          "properties": {
            "role": "point"
          }
        }
      ]
    }
  }
};

// const createLayer = function(source) {
//   return <Layer 
//       type="fill" 
//       id="marker" 
//       paint={{
//         'circle-color': "#ff5200",
//         'circle-stroke-width': 1,
//         'circle-stroke-color': '#fff',
//         'circle-stroke-opacity': 1
//       }}
//     >
//       <Feature coordinates={[-0.132,51.518]}/>
//       <Feature coordinates={[-0.465,51.258]}/>
//     </Layer>
// }


function useForceUpdate() {
  const [, setTick] = useState(0);
  const update = useCallback(() => {
    setTick(tick => tick + 1);
  }, [])
  return update;
};

function arrayMoveElem(array, oldIndex, newIndex) {
  if (newIndex >= array.length) {
    let k = newIndex - array.length + 1;
    while (k--) {
      array.push(undefined);
    }
  }
  array.splice(newIndex, 0, array.splice(oldIndex, 1)[0]);
  return array; // for testing
};

function isEqual(a, b) {
  return a.id === b.id;
};

export {
  createItemSource,
  getCirclePoints,
  createPointSource,
  createCircleSource,
  createFillLayer,
  createSymbolLayer,
  useForceUpdate,
  isEqual
};
