/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

import { zoneTypes } from "../assets/data";
import { RenderStates } from "react-map-gl-draw";
import { isEqual } from "./utils";
const { SELECTED, HOVERED, UNCOMMITTED, CLOSING } = RenderStates;

const GAME_ZONE_STYLE = {
  EDIT_HANDLE: {
    DEFAULT: {
      fill: "rgb(57, 143, 0)",
      fillOpacity: 1,
      stroke: "rgb(255, 255, 255)",
      strokeWidth: 2,
      r: 5
    },
    UNCOMMITTED: {
      fill: "rgb(57, 143, 0)",
      fillOpacity: 1,
      stroke: "rgb(255, 255, 255)",
      strokeWidth: 2,
      r: 7
    }
  },
  FEATURE: {
    DEFAULT: {
      stroke: "rgb(57, 143, 0)",
      strokeWidth: 3,
      fill: "rgba(57, 143, 0)",
      fillOpacity: 0.1,
      // strokeDasharray: "5,5"
    },
    CLOSING: {
      stroke: "rgb(57, 143, 0)",
      strokeWidth: 3,
      fill: "rgb(57, 143, 0)",
      fillOpacity: 0.1,
      strokeDasharray: "5,5"
    }
  }
};

const RESTRICTED_ZONE_STYLE = {
  EDIT_HANDLE: {
    DEFAULT: {
      fill: "rgb(255, 136, 0)",
      fillOpacity: 1,
      stroke: "rgb(255, 255, 255)",
      strokeWidth: 2,
      r: 5
    },
    UNCOMMITTED: {
      fill: "rgb(255, 136, 0)",
      fillOpacity: 1,
      stroke: "rgb(255, 255, 255)",
      strokeWidth: 2,
      r: 7
    }
  },
  FEATURE: {
    DEFAULT: {
      stroke: "rgb(255, 136, 0)",
      strokeWidth: 2,
      fill: "rgb(255, 136, 0)",
      fillOpacity: 0.1
    },
    CLOSING: {
      stroke: "rgb(255, 136, 0)",
      strokeWidth: 2,
      fill: "rgb(255, 136, 0)",
      fillOpacity: 0.3,
      strokeDasharray: "4,2"
    }
  }
};

const ERROR_STYLE = {
  EDIT_HANDLE: {
    fill: "#ff0000",
    fillOpacity: 1,
    stroke: "rgb(255, 255, 255)",
    strokeWidth: 2,
    r: 5
  },
  FEATURE: {
    stroke: "#ff0000",
    strokeWidth: 2,
    fill: "#ff0000",
    fillOpacity: 0.3,
    strokeDasharray: "4,2"
  }
};

export function getEditHandleStyle({ feature, state }, zoneType) {
  if (feature.properties.is_error) {
    return ERROR_STYLE.EDIT_HANDLE;
  }

  let style;
  if (isEqual(feature.properties.zone_type || zoneType, zoneTypes.GAME_ZONE)) {
    style = GAME_ZONE_STYLE.EDIT_HANDLE;
  } else if (isEqual(feature.properties.zone_type || zoneType, zoneTypes.RESTRICTED_ZONE)) {
    style = RESTRICTED_ZONE_STYLE.EDIT_HANDLE;
  }

  switch (state) {
    // case SELECTED:
    // case HOVERED:
    case UNCOMMITTED:
      return style.UNCOMMITTED;

    default:
      return style.DEFAULT;
  }
}

export function getFeatureStyle({ feature, index, state }, zoneType) {
  if (feature.properties.is_error) {
    return ERROR_STYLE.FEATURE;
  }

  let style;
  if (isEqual(feature.properties.zone_type || zoneType, zoneTypes.GAME_ZONE)) {
    style = GAME_ZONE_STYLE.FEATURE;
  } else if (isEqual(feature.properties.zone_type || zoneType, zoneTypes.RESTRICTED_ZONE)) {
    style = RESTRICTED_ZONE_STYLE.FEATURE;
  }

  switch (state) {
    // case SELECTED:
    // case HOVERED:
    // case UNCOMMITTED:
    case CLOSING:
      return style.CLOSING;

    default:
      return style.DEFAULT;
  }
}
