/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

import http from "./http"
import store from "./store";

export default {
  welcome: () => http.get("/"),

  signup: (user) => http.post("/signup", user),
  signin: (user) => http.post("/login", user),
  edit: (user) => http.put(`/users/${store.id}`, user),
  delete: (id) => http.put(`/users/${id}`),

  whoami: () => http.get("/whoami"),

  // Users
  users: () => http.get("/users"),
  user: (id) => http.get(`/users/${id}`),
  friendsOfUser: (id) => http.get(`/users/${id}/friends`),

  // logged user
  myFriends: () => http.get(`/users/${store.id}/friends`),
  addFriend: (id) => http.put(`/users/${store.id}/friends/${id}`),
  removeFriend: (id) => http.delete(`/users/${store.id}/friends/${id}`),

  // invites
  myInvites: () => http.get(`/users/${store.id}/invites`),
  invitesOfUser: (userId) => http.get(`/users/${userId}/invites`),
  inviteUserToGame: (userId, gameId) => http.put(`/users/${userId}/invites/${gameId}`),
  rmUserInviteOfGame: (userId, gameId) => http.put(`/users/${userId}/invites/${gameId}`),

  // games
  games: () => http.get("/partie"),
  createGame: (game) => http.post("/partie", game),
  editGame: (id, game) => http.put(`/partie/${id}`, game),
  deleteGame: (id) => http.delete(`/partie/${id}`),
  gamesGuests: (id) => http.get(`/partie/${id}/invites`),
  gamePlayers: (id) => http.get(`/partie/${id}/players`),
  addUserToGame: (gameId, userId) => http.put(`/partie/${gameId}/players/${userId}`),
  removeUserFromGame: (gameId, userId) => http.delete(`/partie/${gameId}/players/${userId}`),
};