/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

export default {
  _id: "",
  _firstName: "",
  _lastName: "",
  _pseudo: "",
  _email: "",
  _token: "",

  isLogged() {
    return this.token ? true : false;
  },

  set id(str) {
    this._id = Number(str);
    localStorage.setItem("id", str);
  },
  get id() {
    return this._id || Number(localStorage.getItem("id"));
  },

  set firstName(str) {
    this._firstName = str;
    localStorage.setItem("firstName", str);
  },
  get firstName() {
    return this._firstName || localStorage.getItem("firstName");
  },

  set lastName(str) {
    this._lastName = str;
    localStorage.setItem("lastName", str);
  },
  get lastName() {
    return this._lastName || localStorage.getItem("lastName");
  },

  set pseudo(str) {
    this._pseudo = str;
    localStorage.setItem("pseudo", str);
  },
  get pseudo() {
    return this._pseudo || localStorage.getItem("pseudo");
  },

  set email(str) {
    this._email = str;
    localStorage.setItem("email", str);
  },
  get email() {
    return this._email || localStorage.getItem("email");
  },

  set token(str) {
    this._token = str;
    localStorage.setItem("konflictToken", str);
  },
  get token() {
    return this._token || localStorage.getItem("konflictToken");
  },

  clear() {
    localStorage.removeItem("id");
    localStorage.removeItem("firstName");
    localStorage.removeItem("lastName");
    localStorage.removeItem("pseudo");
    localStorage.removeItem("email");
    localStorage.removeItem("konflictToken");
    this._id = "";
    this._firstName = "";
    this._lastName = "";
    this._pseudo = "";
    this._email = "";
    this._token = "";
  }
}