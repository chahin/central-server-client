/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

import axios from "axios";
import { API_URL } from "../assets/data";
import store from './store';


const http = axios.create({
  baseURL: API_URL,
  headers: { 'Content-Type': 'application/json' },
});

http.interceptors.request.use(
  function (config) {
    const token = store.token;
    if (token) config.headers.Authorization = `Bearer ${token}`;
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default http;

