/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

import React from "react";
import { BrowserRouter as Router, Switch, Redirect } from "react-router-dom";
import { ProtectedRoute, RedirectedRoute } from "./router";
import logo from "./logo.svg";
import { Welcome, SignIn, SignUp, Main } from "./components";

function App() {
  return <Router>
    <Switch>
      <RedirectedRoute path="/" exact component={Welcome} />
      <RedirectedRoute path="/signin" component={SignIn} />
      <RedirectedRoute path="/signup" component={SignUp} />
      <ProtectedRoute path={["/home", "/studio", "/settings"]} component={Main} />
    </Switch>
  </Router>
};

export default App;
