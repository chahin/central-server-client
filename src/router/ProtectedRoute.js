/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

import React from "react";
import { Route, Redirect } from "react-router-dom";
import { store } from "../service";

const ProtectedRoute = ({ component: Component, ...rest }) => {
  return <Route
    {...rest}
    render={(props) => store.isLogged()
      ? <Component {...props} />
      : <Redirect to={{
        pathname: "/signin",
        state: {
          info: {
            message: "You must login",
            type: "danger"
          }
        }
      }}
      />
    }
  />
};

export default ProtectedRoute;
