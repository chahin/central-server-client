/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

import React from "react";
import { Route, Redirect } from "react-router-dom";
import { store } from "../service";

const RedirectedRoute = ({ component: Component, ...rest }) => {
  return <Route
    {...rest}
    render={(props) => store.isLogged()
      ? <Redirect to={{ pathname: "/home" }} />
      : <Component {...props} />
    }
  />
};

export default RedirectedRoute;
