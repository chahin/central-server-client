/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

export { default as ProtectedRoute } from './ProtectedRoute';
export { default as RedirectedRoute } from './RedirectedRoute';