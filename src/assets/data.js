/***************************/
/* AUTHOR: GUILLAUME LOUIS */
/***************************/

const API_URL = "https://konflict.princelle.org/api/";

const getImage = function (img) {
  return `https://s3.princelle.org/konflict/${img}`;
};

const colors = {
  ACTIVITY_LAYER: "#feb72b",
  VISIBILITY_LAYER: "#084177",
  ERROR: "red",
  SELECTED: "#faafee"
};

const gameModes = Object.freeze({
  FLAG: {
    id: 0,
    name: "Flag"
  },
  TIME: {
    id: 1,
    name: "Time"
  },
  SUPREMACY: {
    id: 2,
    name: "Supremacy"
  }
});

const modes = Object.freeze({
  NONE: 0,
  ZONE: 1,
  ITEM: 2
});

const zoneTypes = Object.freeze({
  GAME_ZONE: {
    id: "game_zone",
    name: "Game zone"
  },
  RESTRICTED_ZONE: {
    id: "restricted_zone",
    name: "Restricted zone"
  }
});

const itemTypes = Object.freeze({
  FLAG: {
    id: "flag",
    name: "Flag",
    img: "flag.png"
  },
  BARREL: {
    id: "barrel",
    name: "Barrel",
    img: "barrel.png"
  },
  COCAIN: {
    id: "coca",
    name: "Cocain",
    img: "cocaine.png"
  },
  MASK: {
    id: "mask",
    name: "Mask",
    img: "mask.png"
  }
});

export { API_URL, getImage, colors, gameModes, modes, zoneTypes, itemTypes };