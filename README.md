# Central server client

> _Author of this project : **GUILLAUME LOUIS**_

> This project runs properly only on **Chrome** or **Firefox**

Web client containing the game editor.
It allows the user to create a game with a set of options. He can also add game zones, restricted zones, flags and items.

## Installation

#### Clone this project

Use https or ssh link.

```bash
git clone https://git.unistra.fr/konflict/central-server-client.git
```

#### Install dependencies

To install dependencies, run the following command.

```bash
npm i
```

#### Set up mapbox
In order to use mapbox, a mapbox token is required. In order to have one, create an account on [mapbox's official website](https://www.mapbox.com).

Once the mapbox token available, make sure to create a `.env` file at the project root with the following content.

```js
MapboxAccessToken=[yourTokenHere] //without brackets
```

If you want to change the port on which the app is running, please add the following line. (default is 3000).

```js
PORT=[yourPort] // without brackets
```

#### Test the project
To run the project, make sure the running port is available and run the following command.

```bash
npm start
```
